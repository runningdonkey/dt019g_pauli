//------------------------------------------------------------------------------
// Lab4.cpp Objektbaserad programmering i C++
//------------------------------------------------------------------------------

#include "Lab4.h"
#include <iostream>

/**
 * Main program
 */
int main()
{
    std::cout << getAssignmentInfo() << std::endl;
    return 0;
}

