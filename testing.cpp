//
// Created by pauli on 4/12/19.
//

#include "proj.h"
#include "Time.h"
#include "Song.h"
#include "Menu.h"
#include <Album.h>

#include <iostream>
#include <MenuItem.h>
#include <Jukebox.h>

/*   TEST TIME-CLASS  */

// Test Time set and get
Time tid;
tid.setHours(4);
tid.setMinutes('a');
tid.setSeconds(34);
std::cout << tid.getHours() << ":";
std::cout << tid.getMinutes() << ":";
std::cout << tid.getSeconds();

// Test Time-constructor
Time tid2(4728);
std::cout << std::endl;
std::cout << tid2.getHours() << ":";
std::cout << tid2.getMinutes() << ":";
std::cout << tid2.getSeconds();

std::cout << std::endl;
std::cout << tid2.getTimeInSeconds();

// Test overload of addition
Time addedtime = tid + tid2;
std::cout << std::endl;
std::cout << addedtime.getHours() << ":";
std::cout << addedtime.getMinutes() << ":";
std::cout << addedtime.getSeconds();

// Test overload of less-than
std::cout << std::endl;
std::cout << (tid < tid) << std::endl;
std::cout << (tid < tid2) << std::endl;
std::cout << (tid2 < tid) << std::endl;

// Test overload of equal-to (==)
std::cout << std::endl;
std::cout << (tid == tid) << std::endl;
std::cout << (tid == tid2) << std::endl;
std::cout << (tid2 == tid) << std::endl;

// Test overload of output-operator (<<)
std::cout << tid << std::endl;
std::cout << tid2 << std::endl;

// Test overload of input-operator (seconds >> time-object)
Time tid3;
std::cout << "Please enter time in seconds: " << std::endl;
std::cin >> tid3;
std::cout << std::endl;
std::cout << tid3.getHours() << ":";
std::cout << tid3.getMinutes() << ":";
std::cout << tid3.getSeconds() << std::endl;
std::cout << tid3.getTimeInSeconds() << std::endl;

/*    TEST SONG-CLASS  */

// Test Song-constructor
Song song("Singing the high C", "C. Lipmann", tid);
std::cout << song.getTitle() << std::endl;
std::cout << song.getArtist() << std::endl;
std::cout << song.getLength() << std::endl;

// Test song-overload of output-operator (<<)
std::cout << song << std::endl;

// Test song-overload of input-operator (>>)
Song song1 = song;
//std::cin >> song1;
std::cout << song1 << std::endl;

/*    TEST ALBUM-CLASS  */
std::cout << "** Test Album-class **" << std::endl;

// Test album default constructor
Album album;

// Test Album-constructor
std::vector<Song> liste;
liste.push_back(song);
liste.push_back(song1);
Album album1("The great Album", liste );

// Test Album overload output-operator (<<)
std::cout << album1 << std::endl;
std::cout << album << std::endl;

// Test album setName and add to album
album.setAlbumName("Whoww hits");
album.addToAlbum(song);
std::cout << album << std::endl;

// Test album overload input-operator (>>)
Album album3 = album;
//    std::cin >> album3;
std::cout << "Output album 3" << std::endl;
std::cout << album3 << std::endl;

// Test Album set songlist
Album album4;
liste = album3.getASongList();
album4.setAlbumSongList(liste);
std::cout << "Output album 4" << std::endl;
std::cout << album4 << std::endl;

// Test getTotalTime and compare (less-than)
std::cout << "Total time of album: ";
std::cout << album.getTotalTime() << std::endl;

std::cout << "Total time of album 3: ";
std::cout << album3.getTotalTime() << std::endl;

std::cout << "Is album less than album3?" << std::endl;
if (album < album3) {std::cout << "Yes";}
else {std::cout << "No";}
std::cout << std::endl;


// TEST jukebox and menues

Jukebox jukebox;
Menu tmpmenu = jukebox.getMenu();
tmpmenu.setMenuTitle("JUKEBOX");

// Run Menu
bool again = true;
do
{
tmpmenu.printMenuItems();
switch (tmpmenu.getMenuChoice("Please choose an option."))
{
case 1: cout << "hej";
break;

}
}while(again);
