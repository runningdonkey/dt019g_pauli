//------------------------------------------------------------------------------
// NameTest.cpp Objektbaserad programmering i C++
//------------------------------------------------------------------------------

#include "../include/Name.h"

int main()
{
    // Testing default constructor
    Name empty;
    cout << "Show object created with default constructor" << endl;
    cout << "Firstname: " << empty.getFirstName() << endl;
    cout << "Lastname: " << empty.getLastName() << endl;


    // Testing constructor with initializing
    Name mig("Pauli", "Baadsager");
    cout << endl;
    cout << "Show object constructed with intializing" << endl;
    cout << "Firstname: " << mig.getFirstName() << endl;
    cout << "Lastname: " << mig.getLastName() << endl;

    // TESTING SETFIRSTNAME() AND SETLASTNAME()
    mig.setFirstName("Hans");
    mig.setLastName("Petersen");
    cout << endl;
    cout << "Show object changed with member-functions" << endl;
    cout << "Firstname: " << mig.getFirstName() << endl;
    cout << "Lastname: " << mig.getLastName() << endl;
}
