/*
 * =====================================================================================
 *        Class:  Name
 *  Description:  Definitionfile to the class Name. The class handles first- and lastname.
 *   Created by:  Pauli Baadsager on 2/8/19.
 * =====================================================================================
 */

#include "Name.h"

// Default constructor
Name::Name()
{
    // Don't need anything because default constructor of a string is all ready defined.
    firstName = "NN";
    lastName = "NN";
}

// Destructor
Name::~Name()
{
    // Don't need anything because default destructor of a string is all ready defined.
}

// Constructor for initializing object
Name::Name(string pFirstName, string pLastName)
{
    firstName = pFirstName;
    lastName = pLastName;
}

// DEFINITION OF MEMBER FUNCTIONS (METHODS)

// Set the firstname of a Name-object
void Name::setFirstName(string pFirstName) {
    firstName = pFirstName;
}

// Set the lastname of a Name-object
void Name::setLastName(string pLastName) {
    lastName = pLastName;
}

// Returns the firstname of a Name-object
string Name::getFirstName() const {
    return firstName;
}

// Returns the lastname of a Name-object
string Name::getLastName() const {
    return lastName;
}

