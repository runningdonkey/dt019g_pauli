//------------------------------------------------------------------------------
// AddressTest.cpp Objektbaserad programmering i C++
//------------------------------------------------------------------------------

#include <iostream>
#include "Address.h"

using namespace std;

/**
 * Main program
 */
int main()
{
    cout << "Laboration 2, test program for class Address" << endl;

    // Testing default construction
    cout << "Show default constructed object";
    Address empty;
    cout << empty.getStreet() << endl;
    cout << empty.getPostcode() << endl;
    cout << empty.getCity() << endl;


    // Testing construction with initializing and get methods
    cout << "Show object constructed with initializing" << endl;
    Address mig("Dynemosen 14", "2321", "Holmegaard");
    cout << "Street: " << mig.getStreet() << endl;
    cout << "Postcode: " << mig.getPostcode() << endl;
    cout << "City: " << mig.getCity() << endl;


    // Testing set methods
    mig.setStreet("Hollandsgade 234");
    mig.setPostcode("4011");
    mig.setCity("København");
    cout << endl;
    cout << "Show object changed with member-functions" << endl;
    cout << "Street: " << mig.getStreet() << endl;
    cout << "Postcode: " << mig.getPostcode() << endl;
    cout << "City: " << mig.getCity() << endl;
}

