//
// Created by pauli on 2/9/19.
//


#include <Person.h>

#include "Person.h"

Person::Person() {
    persNr = "NN";
    skoNr = 0;
}

Person::Person(Name pName, Address pAddress, string pPersNr, int pSkoNr) {
    name = pName;
    address = pAddress;
    persNr = pPersNr;
    skoNr = pSkoNr;
}

void Person::setName(Name aName) {
    name = aName;
}

void Person::setAddress(Address pAddress) {
    address = pAddress;
}

void Person::setPersNr(string pPersNr) {
    persNr = pPersNr;
}

void Person::setSkoNr(int pSkoNr) {
    skoNr = pSkoNr;
}


