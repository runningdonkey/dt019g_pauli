//
// Functions to handle user interface. Like show menu and shows data.
// Takes user-input to create person and add to list
//
// Created by pauli on 2/24/19.
//

#include "Menu.h"

//------------------------------------------------------------------------------
// Description: Show menu and ask user to choose entry. Copied from PEFunkBibl.
//       Input: List of strings and an integer with entries in list
//      Output: Returns a char, should be the user choice
//------------------------------------------------------------------------------
char menu(const string text[], int itemNum)
{
    for(int i=0; i<itemNum; i++)
        cout << setw(4) << (i+1) << ". " << text[i] << endl;
    cout << endl << "Choose alternative: ";
    char ch;
    cin >> ch;
    cin.get();
    return ch;
}

//------------------------------------------------------------------------------
// Description: Ask user to input a person. That person is added to list.
//       Input: List with persons (a vector)
//      Output: -
//------------------------------------------------------------------------------
void pushData(vector<Person> &personList)
{
    string firstName, lastName, street, postcode, city, persNr;
    int skoNr;

    // Ask user for data
    cout << "Please enter firstname: ";
    cin >> firstName;

    cout << "Please enter lastname: ";
    cin >> lastName;

    cout << "Please enter street: ";
    cin >> street;

    cout << "Please enter postcode: ";
    cin >> postcode;

    cout << "Please enter city: ";
    cin >> city;

    cout << "Please enter personnumber: ";
    cin >> persNr;

    cout << "Please enter shoesize: ";
    cin >> skoNr;

    // Create Name-object
    Name name;
    name = Name(firstName, lastName);

    // Create Address-object
    Address address;
    address = Address(street, postcode, city);

    // Create Person-object
    Person person;
    person = Person(name, address, persNr, skoNr);

    // Push Person-object to list (vector)
    personList.push_back(person);

}


//------------------------------------------------------------------------------
// Description: Show data on screen for all persons in list (vector)
//       Input: List with persons (a vector)
//      Output: -
//------------------------------------------------------------------------------
void showData(vector<Person> &personList)
{
    unsigned i = 1;
    for(auto const &e : personList) {
        // Output header for each person
        cout << "Entry: " << i << endl;
        i++;

        // Output name
        Name name = e.getName();
        cout << "Firstname: " << name.getFirstName() << endl;
        cout << "Lastname: " << name.getLastName() << endl;

        // Output address
        Address address;
        address = e.getAddress();
        cout << "Street: " << address.getStreet() << endl;
        cout << "Postcode: " << address.getPostcode() << endl;
        cout << "City: " << address.getCity() << endl;

        // Output shoenumber and personnumber
        cout << "Personnumber: " << e.getPersNr() << endl;
        cout << "Shoenumber: " << e.getSkoNr() << endl;
        cout << endl;
    }
}
