//------------------------------------------------------------------------------
// PersonTest.cpp Objektbaserad programmering i C++
// Created by pauli on 2/9/19.
//------------------------------------------------------------------------------

#include "Person.h"

int main()
{
    // Testing initialization of object with constructor given parameters
    Name myName("Pauli", "Baadsager");
    Address myAddress("Spilgade 23", "3432", "Jyllinge");
    string myPersNr = "M-2001";
    int mySkoNr = 44;
    Person me(myName, myAddress, myPersNr, mySkoNr);
    //Person me(Name("Pauli", "Baadsager"), Address("Toppen 338", "3425", "Hanstholm"), "M-3022", 44);


    // Testing default construction of object and testing all member set-functions
    Person other;
    other.setName(Name("Stefan", "Baadsager"));
    other.setAddress(Address("Torvegade 12", "2311", "Tybjerg"));
    other.setPersNr("M-0902");
    other.setSkoNr(44);

    // Testing all member get-functions
    cout << "Show object created with default constructor and set-functions" << endl;
    cout << "Firstname:  " << other.getName().getFirstName() << endl;
    cout << "Lastname:   " << other.getName().getLastName() << endl;
    cout << "Adr.Street: " << other.getAddress().getStreet() << endl;
    cout << "Adr.Postnr: " << other.getAddress().getPostcode() << endl;
    cout << "Adr.City:   " << other.getAddress().getCity() << endl;
    cout << "PersNr:     " << other.getPersNr() << endl;
    cout << "SkoNr:      " << other.getSkoNr() << endl;

    cout << endl;

    cout << "Show object initialized with constructor given parameters" << endl;
    cout << "Firstname:  " << me.getName().getFirstName() << endl;
    cout << "Lastname:   " << me.getName().getLastName() << endl;
    cout << "Adr.Street: " << me.getAddress().getStreet() << endl;
    cout << "Adr.PostNr: " << me.getAddress().getPostcode() << endl;
    cout << "Adr.City:   " << me.getAddress().getCity() << endl;
    cout << "PersNr:     " << me.getPersNr() << endl;
    cout << "SkoNr:      " << me.getSkoNr() << endl;

    // Testing changing with set-function
    me.setSkoNr(46);
    cout << "SkoNr:      " << me.getSkoNr() << endl;
}
