/*
 * =====================================================================================
 *        Class:  Address
 *  Description:  Definition of the class Address. Handles street, postcode and city.
 *   Created by:  Pauli Baadsager on 2019-02-08
 * =====================================================================================
 */

#include "Address.h"

// Default constructor
Address::Address() {
    // Nothing needed because std::string has already a default constructor
    street = "Unknown";
    postcode = "Unknown";
    city = "Unknown";
}

// Destructor
Address::~Address() {
    // Nothing needed because std::string has already a default destructor
}

// Constructor with initializing
Address::Address(string pStreet, string pPostcode, string pCity) {
    street = pStreet;
    postcode = pPostcode;
    city = pCity;
}

// SET METHODS
void Address::setStreet(string pStreet) {
    street = pStreet;
}

void Address::setPostcode(string pPostcode) {
    postcode = pPostcode;
}

void Address::setCity(string pCity) {
    city = pCity;
}

// GET METHODS
string Address::getStreet() const {
    return street;
}

string Address::getPostcode() const {
    return postcode;
}

string Address::getCity() const {
    return city;
}


