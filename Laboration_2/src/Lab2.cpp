//------------------------------------------------------------------------------
// Lab2.cpp Objektbaserad programmering i C++
// Assignment 3.
//------------------------------------------------------------------------------

#include "Menu.h"

using std::vector;
/**
 * Main program
 */
int main()
{
    const int ITEMNUM = 3;
    string menuItems[ITEMNUM] = {"input data for a person",
                                 "Show alle persons in list",
                                 "Quit"};

    // Create empty vector as list of persons
    vector<Person> personList;

    // Add 3 persons to vector before menu is run
    //TODO: make this
    Person person1, person2, person3;
    person1 = Person( Name("Pauli", "Baadsager"), Address("Spoletoppen 34", "4321", "Herluftmagle"), "M20123", 43);
    person2 = Person( Name("Ole", "Hansen"), Address("transbjergvej 4", "3213", "Gunnersmark"), "M32124", 34);
    person3 = Person( Name("Yrsa", "Bonne"), Address("Husgade 3A", "5871", "Hullumhej"), "F04123", 34);

    personList.push_back(person1);
    personList.push_back(person2);
    personList.push_back(person3);


    // Run the menu
    bool go = true;
    do {
        switch ( menu(menuItems, ITEMNUM) ) {
            case '1': //input data
            pushData(personList);
                break;
            case '2': // show all
            showData(personList);
                break;
            case '3': go = false;
        }
    } while(go);
}

