/*
 * =====================================================================================
 *        Class:  Address
 *  Description:  Headerfile to the class Address
 *   Created by:  Pauli Baadsager on 2019-02-08
 * =====================================================================================
 */

#ifndef DT019G_ADDRESS_H
#define DT019G_ADDRESS_H

#include <string>

//using namespace std;
using std::string;

class Address {

private:
    string street;
    string postcode;
    string city;

public:

    // Default constructor
    Address();

// Constructor for initializing object
    Address(string pStreet, string pPostcode, string pCity);

    // Destructor
    ~Address();

    // SET METHODS
    void setStreet(string pStreet);
    void setPostcode(string pPostcode);
    void setCity(string pCity);

    // GET METHODS
    string getStreet() const;
    string getPostcode() const;
    string getCity() const;
};



#endif //DT019G_ADDRESS_H
