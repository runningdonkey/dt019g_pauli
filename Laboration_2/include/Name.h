/*
 * =====================================================================================
 *        Class:  Name
 *  Description:  Headerfile to the class Name
 *   Created by:  Pauli Baadsager on 2/8/19.
 * =====================================================================================
 */

#ifndef DT019G_NAME_H
#define DT019G_NAME_H

#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::endl;


class Name
{

private:
    string firstName;
    string lastName;

public:
    // Default constructor
    Name();

// Constructor for initializing object
    Name(string pFirstName, string pLastName);

    // Destructor
    ~Name();

    // SET METHODS
    void setFirstName(string pFirstName);
    void setLastName(string pLastName);

    // GET METHODS
    std::string getFirstName() const;
    std::string getLastName() const;
};

#endif //DT019G_NAME_H
