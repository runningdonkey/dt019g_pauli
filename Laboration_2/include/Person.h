//
// Created by pauli on 2/9/19.
//

#ifndef DT019G_PERSON_H
#define DT019G_PERSON_H

#include "Name.h"
#include "Address.h"

class Person {

private:
    Name name;
    Address address;
    string persNr;
    int skoNr;      // TODO: Comment in report. Make this unsigned jvf. bogen.

public:
    // Default constructor
    Person();
    Person(Name pName, Address pAddress, string pPersNr, int pSkoNr);

    // Set functions
    void setName(Name pName);
    void setAddress(Address pAddress);
    void setPersNr(string pPersNr);
    void setSkoNr(int pSkoNr);     // TODO: remove double aa, if works

    // Get functions
    Name getName() const { return name; }
    Address getAddress() const { return address; }
    string getPersNr() const { return persNr; }
    int getSkoNr() const { return skoNr; }

};


#endif //DT019G_PERSON_H
