//
// Created by pauli on 2/24/19.
//

#ifndef DT019G_MENU_H
#define DT019G_MENU_H

#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

#include "Person.h"

using std::setw;
using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::vector;


char menu(const std::string text[], int itemNum);

void pushData(vector<Person> &personList);

void showData(vector<Person> &personList);

#endif //DT019G_MENU_H
