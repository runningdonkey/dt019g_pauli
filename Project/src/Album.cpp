//
// Created by pauli on 3/31/19.
//

#include "Album.h"

using std::endl;
using std::stoul;

// Default constructor
Album::Album()
{
    aName = "-unknown album-";
    // No need to set aSongs, because empty vector is created by default constructor of std::vector

}

// Constructor for initializing object
Album::Album(string name, vector<Song> songs)
{
    aName = name;
    aSongs = songs;
}

// Destructor
Album::~Album() = default;

// DEFINITION OF MEMBER FUNCTIONS (METHODS)

// Set functions
void Album::setAlbumName(string name)
{
    aName = name;
}

void Album::setAlbumSongList(vector<Song> songs)
{
    aSongs = songs;
}

// GET FUNCTIONS
// Get total time (as a time-object) of an album
Time Album::getTotalTime() const
{
    Time total;
    vector<Song> list = this -> getASongList();
    for (auto e : list)
    {
        total = total + e.getLength();
    }
    return total;
}

// OVERLOADED OPERATORS
// Compare (less-than)
bool Album::operator< (const Album &album) const
{
    // Get time-object from both implicit- and explicit parameter
    Time time1 = this -> getTotalTime();
    Time time2 = album.getTotalTime();

    // Convert time to seconds
    int seconds1 = time1.getTimeInSeconds();
    int seconds2 = time2.getTimeInSeconds();

    // Return bool
    return (seconds1 < seconds2);

}

// Add a song to album
void Album::addToAlbum(Song &song)
{
    aSongs.push_back(song);
}

// Output operator overload
std::ostream &operator<< (std::ostream &os, const Album &album)
{
    // get vector with songs in album
    vector<Song> songlist;
    songlist = album.getASongList();

    // get length of songlist
    unsigned long count;
    count = songlist.size();

    // write album header to stream
    os << album.getAlbumName()
       << endl
       << count
       << endl;

    // write every song in songlist to stream
    for (auto e : songlist) { os << e << endl; }
    return os;
}

// Input operator overload
std::istream &operator>> (std::istream &is, Album &album)
{
    // get first line as albumname
    string str;
    getline(is, str);
    album.setAlbumName(str);

    // get number of songs in album
    unsigned long count;
    getline(is, str);
    count = stoul(str, nullptr, 10);    // cast string to unsigned long

    // add all songs to album
    Song tempsong;
    for (unsigned long i; i < count; i++)
    {
       is >> tempsong;
       album.addToAlbum(tempsong);
    }
}




