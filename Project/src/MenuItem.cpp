//
// Created by pauli on 4/12/19.
//

#include "MenuItem.h"


MenuItem::MenuItem(string pMenutext, bool pValbar)
{
    menutext = pMenutext;
    valbar = pValbar;
}

void MenuItem::setValbar(bool enabled)
{
    valbar = enabled;
}


void MenuItem::setMenutext(string &pMenutext)
{
    menutext = pMenutext;
}
