//
// Created by pauli on 3/23/19.
//

#include "Song.h"

// Default constructor
Song::Song()
{
    sTitle = "unknown";
    sArtist = "unknown";
    sLength = Time(0);
}

// Constructor for initializing object
Song::Song(string title, string artist, Time length)
{
    sTitle = title;
    sArtist = artist;
    sLength = length;
}

// Destructor
Song::~Song() = default;

// DEFINITION OF MEMBER FUNCTIONS (METHODS)

// SET FUNCTIONS
// Set the title of a Song-object.
void Song::setTitle(string title) {
    sTitle = title;
}

// Set the artist of a Song-object.
void Song::setArtist(string artist) {
    sArtist = artist;
}

// Set the length of a Song-object.
void Song::setLength(Time length) {
    sLength = length;
}

// Output operator overload (output time as defined by Time-class)
std::ostream &operator<< (std::ostream &os, const Song &song)
{
    os << song.getTitle()
    << DELIMITER
    << song.getArtist()
    << DELIMITER
    << song.getLength().getTimeInSeconds();
    return os;
}

// Input operator overload (input title and artist as strings and time as integer seconds)
std::istream &operator>> (std::istream &is, Song &song)
{
    string temp;
    getline(is,temp,DELIMITER); // Read in first column
    song.setTitle(temp);

    getline(is,temp,DELIMITER); // Read in second column
    song.setArtist(temp);

    int tempInt;
    is >> tempInt;      // Read in third column an integer
    is.get();
    Time sLength(tempInt);  // construct time-object from seconds
    song.setLength(sLength);

}
