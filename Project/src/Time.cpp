//
// Created by pauli on 3/22/19.
//

#include "Time.h"

// Default constructor
Time::Time()
{
    // Set default time hh:mm:ss to 00:00:00
    hours = 0;
    minutes = 0;
    seconds = 0;
}

// Constructor for initializing object
Time::Time(int timeInSeconds)
{
    // Remove possible negative time
    if (timeInSeconds < 0) { timeInSeconds = 0; }

    hours = timeInSeconds/SEC_PR_HOUR;
    timeInSeconds %= SEC_PR_HOUR;
    minutes = timeInSeconds/SEC_PR_MINUTE;
    timeInSeconds %= SEC_PR_MINUTE;
    seconds = timeInSeconds;
}

// Destructor
Time::~Time() = default;

// DEFINITION OF MEMBER FUNCTIONS (METHODS)

// SET FUNCTIONS
// Set the hours of a Time-object. Sets only if parameter is valid interval, and 24 = 0.
void Time::setHours(int pHours) {
   if (0 <= pHours && pHours <= 24)
   {
       hours = (pHours%24);
   }
}

// Set the minutes of a Time-object. Sets only if parameter is valid interval.
void Time::setMinutes(int pMinutes) {
    if (0 <= pMinutes && pMinutes < 60)
    {
        minutes = pMinutes;
    }
}

// Set the seconds of a Time-object. Sets only if parameter is valid interval.
void Time::setSeconds(int pSeconds) {
    if (0 <= pSeconds && pSeconds < 60)
    {
        seconds = pSeconds;
    }
}

int Time::getTimeInSeconds() const {
    int hoursInSeconds = SEC_PR_HOUR * this -> getHours();
    int minutesInSeconds = SEC_PR_MINUTE * this -> getMinutes();
    int seconds = this -> getSeconds();
    int timeInSeconds = hoursInSeconds + minutesInSeconds + seconds;
    return timeInSeconds;
}

// OVERLOADED OPERATORS
// Addition
Time Time::operator+ (const Time &time) const
{
    // get total time in sec. of implicit (left) parameter
    int time1 = this -> getTimeInSeconds();

    // get total time in sec. of explicit (right) parameter
    int time2 = time.getTimeInSeconds();

    int sum = time1 + time2;

    // construct Time-object and return (based on total seconds)
    Time tmp(sum);
    return tmp;
}

// Compare (less-than)
bool Time::operator< (const Time &time) const
{
    // get total time in sec. of implicit (left) parameter
    int time1 = this -> getTimeInSeconds();

    // get total time in sec. of explicit (right) parameter
    int time2 = time.getTimeInSeconds();

    // Compare
    return (time1 < time2);
}

// Compare (equal)
bool Time::operator== (const Time &time) const
{
    // get total time in sec. of implicit (left) parameter
    int time1 = this -> getTimeInSeconds();

    // get total time in sec. of explicit (right) parameter
    int time2 = time.getTimeInSeconds();

    // Compare
    return (time1 == time2);
}

// Output operator overload (output time in seconds)
std::ostream &operator<< (std::ostream &os, const Time &time)
{
    os << time.getTimeInSeconds();
    return os;
}

// Input operator overload (input time in seconds)
std::istream &operator>> (std::istream &is, Time &time)
{
    int tempSec;
    is >> tempSec;
    is.get();
    time = Time(tempSec);
}
