//
// Created by pauli on 4/12/19.
//

#include <Menu.h>
#include "Menu.h"


void Menu::addItem(MenuItem &pMenuItem)
{
   options.push_back(pMenuItem);
}

void Menu::printMenuItems()
{
    for (auto e : options)
    {
        if (e.getValbar()) {
            cout << e.getMenuText() << endl;
        }
    }
}

int Menu::getMenuChoice(string question)
{
    int sel;
    while ( true ) {
        cout << question ;
        if (cin >> sel) {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            break;
        } else {
            cout << "Please enter a valid integer" << endl;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
        }
    }
    return sel;
}

void Menu::setMenuTitle(string title)
{
    menuTitle = title;
}
