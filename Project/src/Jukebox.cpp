//
// Created by pauli on 4/12/19.
//

#include "Jukebox.h"

Jukebox::Jukebox() {
    // albums = empty vector
    MenuItem file("File",true), albumadd("Add album",false), albumdelete("Delete album",false);
    menu.addItem(file);
    menu.addItem(albumadd);
    menu.addItem(albumdelete);
}

void Jukebox::run() {

}