//------------------------------------------------------------------------------
// main.cpp Objektbaserad programmering i C++
//------------------------------------------------------------------------------

#include "proj.h"
#include "Time.h"
#include "Song.h"
#include "Menu.h"
#include <Album.h>

#include <iostream>
#include <MenuItem.h>
#include <Jukebox.h>

int main() {
    Jukebox jukebox;
    jukebox.run();

}