//
// Created by pauli on 4/12/19.
//

#ifndef DT019G_JUKEBOX_H
#define DT019G_JUKEBOX_H

#include "Album.h"
#include "Menu.h"


class Jukebox {
private:
    vector<Album> albums;
    Menu menu;

public:

    // Default constructor
    Jukebox();

    // Get functions
    Menu getMenu() const { return menu; }

    void run();

};


#endif //DT019G_JUKEBOX_H
