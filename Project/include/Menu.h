//
// Created by pauli on 4/12/19.
//

#ifndef DT019G_MENU_H
#define DT019G_MENU_H

#include <limits>
#include <iostream>
#include <vector>
#include <string>
#include "MenuItem.h"

using std::numeric_limits;
using std::streamsize;

using std::vector;
using std::string;
using std::cout;
using std::cin;
using std::endl;

class Menu {

private:
    vector<MenuItem> options;
    string menuTitle;

public:
    void addItem(MenuItem &pMenuItem);
    void printMenuItems();

    void setMenuTitle(string title);

    int getMenuChoice(string question);



};


#endif //DT019G_MENU_H
