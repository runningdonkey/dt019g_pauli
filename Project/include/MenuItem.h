//
// Created by pauli on 4/12/19.
//

#ifndef DT019G_MENUITEM_H
#define DT019G_MENUITEM_H

#include <string>
using std::string;

class MenuItem {

private:
    string menutext;
    bool valbar;

public:
    // constructor and destructor
    MenuItem(string pMenytext, bool pValbar);

    // set functions
    void setValbar(bool enabled);
    void setMenutext(string &pMenutext);


    // get functions
    string getMenuText() const { return menutext; }
    bool getValbar() const { return valbar; }


};


#endif //DT019G_MENUITEM_H
