//
// Created by pauli on 3/31/19.
//

#ifndef DT019G_ALBUM_H
#define DT019G_ALBUM_H

#include <string>
#include <vector>
#include <iostream>
#include "Song.h"

//using namespace std;
using std::string;
using std::vector;

class Album {

private:
    string aName;
    vector<Song> aSongs;

public:
    // Default constructor
    Album();

// Constructor for initializing object
explicit Album(string name, vector<Song> songs);

    // Destructor
    ~Album();

    // SET METHODS
    void setAlbumName(string name);
    void setAlbumSongList(vector<Song> songs);

    // Add a song to album
    void addToAlbum(Song &song);

    // GET METHODS
    string getAlbumName() const { return aName; }
    vector<Song> getASongList() const { return aSongs; }
    Time getTotalTime() const;

    // Overloads
    // TODO: Nice. Add overload for assign operator +=
    bool operator< (const Album &album) const;

/*    Time operator+ (const Time &time) const;
    bool operator== (const Time &time) const;*/

};

// OVERLOAD FREE FROM CLASS
// Output operator
std::ostream &operator<< (std::ostream &os, const Album &album);

// Input operator
std::istream &operator>> (std::istream &is, Album &album);

#endif //DT019G_ALBUM_H
