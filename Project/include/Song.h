//
// Created by pauli on 3/23/19.
//

#ifndef DT019G_SONG_H
#define DT019G_SONG_H

constexpr char DELIMITER='|';

#include "Time.h"
#include <string>


//using namespace std;
using std::string;

class Song {

private:
    string sTitle;
    string sArtist;
    Time sLength;

public:
    // Default constructor
    Song();

// Constructor for initializing object
    Song(string title, string artist, Time length);

    // Destructor
    ~Song();

    // SET METHODS
    void setTitle(string title);
    void setArtist(string artist);
    void setLength(Time length);

    // GET METHODS
    string getTitle() const { return sTitle; }
    string getArtist() const { return sArtist; }
    Time getLength() const { return sLength; }



};

// OVERLOAD FREE FROM CLASS
// Output operator
std::ostream &operator<< (std::ostream &os, const Song &song);

// Input operator
std::istream &operator>> (std::istream &is, Song &song);

#endif //DT019G_SONG_H
