//
// Created by pauli on 3/22/19.
//

#ifndef DT019G_TIME_H
#define DT019G_TIME_H
constexpr unsigned SEC_PR_HOUR = 3600;
constexpr unsigned SEC_PR_MINUTE = 60;

#include <iostream>

class Time {

private:
    int hours;
    int minutes;
    int seconds;

public:
    // Default constructor
    Time();

// Constructor for initializing object
    explicit Time(int timeInSeconds);

    // Destructor
    ~Time();

    // SET METHODS
    void setHours(int pHours);
    void setMinutes(int pMinutes);
    void setSeconds(int pSeconds);

    // GET METHODS
    int getHours() const { return hours; }
    int getMinutes() const { return minutes; }
    int getSeconds() const { return seconds; }

    // Get total time in seconds
    int getTimeInSeconds() const;

    // Overloads
    // TODO: Nice. Add overload for assign operator +=
    Time operator+ (const Time &time) const;
    bool operator< (const Time &time) const;
    bool operator== (const Time &time) const;
};

// OVERLOAD FREE FROM CLASS
// Output operator
std::ostream &operator<< (std::ostream &os, const Time &time);

// Input operator
std::istream &operator>> (std::istream &is, Time &time);

#endif //DT019G_TIME_H
