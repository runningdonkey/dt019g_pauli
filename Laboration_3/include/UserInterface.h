//
// Created by pauli on 4/7/19.
//

#ifndef DT019G_USERINTERFACE_H
#define DT019G_USERINTERFACE_H

#include "PersonList.h"
#include <vector>
#include <limits>

using std::vector;
using std::cin;
using std::numeric_limits;
using std::streamsize;

char constexpr DELIM_SUBSTITUTE = ' ';

class UserInterface {

private:
    PersonList personlist;

    void readFromFile();
    void saveOnFile();

    int showMenu();
    int getInteger(string question);


    Person inputPerson();

public:
    void run();

};


#endif //DT019G_USERINTERFACE_H
