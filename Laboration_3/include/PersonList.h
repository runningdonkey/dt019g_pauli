//
// Created by pauli on 2/10/19.
//

#ifndef DT019G_PERSONLIST_H
#define DT019G_PERSONLIST_H

#include "Person.h"
#include <vector>
#include <algorithm>
#include <fstream>
#include <sstream>

using std::ifstream;
using std::ofstream;

class PersonList {

private:
    std::vector<Person> persons;
    std::string fileName;

public:

    // Default constructor
    PersonList();

    // Get member functions
    string getFileName() const { return fileName; }
    size_t getListLength() const;
    bool getPerson(int idx, Person &person) const;



    // Set member functions
    void setFileName(string &pFileName);

    // Manipulate member functions
    void addPerson(Person const &person);

    // SORTING
    void sortName();
    void sortPersNr();
    void sortSkoNr();

    // IN-OUT FILE
    void readFromFile();
    void writeToFile() const;

};

// OVERLOAD FREE FROM CLASS
// Output operator
std::ostream &operator<< (std::ostream &os, const PersonList &personlist);


#endif //DT019G_PERSONLIST_H
