/*
 * =====================================================================================
 *        Class:  Name
 *  Description:  Headerfile to the class Name
 *   Created by:  Pauli Baadsager on 2/8/19.
 * =====================================================================================
 */

#ifndef DT019G_NAME_H
#define DT019G_NAME_H

#include <iostream>
#include <string>

constexpr char DELIM = '|';

using std::string;
using std::cout;
using std::endl;


class Name
{

private:
    string firstName;
    string lastName;

public:
    // Default constructor
    Name();

// Constructor for initializing object
    Name(string pFirstName, string pLastName);

    // Destructor
    ~Name();

    // SET METHODS
    void setFirstName(string pFirstName);
    void setLastName(string pLastName);

    // GET METHODS
    std::string getFirstName() const;
    std::string getLastName() const;


    // Overloads
    bool operator== (const Name &name) const;
    bool operator< (const Name &name) const;
/*    Name operator+ (const Time &time) const;
    bool operator< (const Time &time) const;*/

};

// OVERLOAD FREE FROM CLASS
// Output operator
std::ostream &operator<< (std::ostream &os, const Name &name);

// Input operator
std::istream &operator>> (std::istream &is, Name &name);

#endif //DT019G_NAME_H
