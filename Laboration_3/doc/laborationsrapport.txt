LABORATIONSRAPPORT TIL LABORATION 3
"Introduktion till programmering i C++” och "Objektbaserad programmering i C++"


LABORATIONSDATA
Pauli Baadsager (log-in name: paba1801)
Laboration 3. Handed-in: friday 12. april 2019
Created using (IDE): Clion 

FILER
3 directories, 14 files

├── doc
│   ├── laborationsrapport.odt
│   ├── laborationsrapport.pdf
│   └── laborationsrapport.txt
├── include
│   ├── Address.h
│   ├── Name.h
│   ├── Person.h
│   ├── PersonList.h
│   └── UserInterface.h
└── src
    ├── Address.cpp
    ├── lab3.cpp
    ├── Name.cpp
    ├── Person.cpp
    ├── PersonList.cpp
    └── UserInterface.cpp

SLUTSATSER OCH KOMMENTARER
Nice to make get-functions return void, and handle the return as a reference parameter (i case of empty request result). Found to have issues reading from file, it might be an OS-problem.
