/*
 * =====================================================================================
 *        Class:  Address
 *  Description:  Definitionfile to the class Address
 *   Created by:  Pauli Baadsager on 2/8/19.
 * =====================================================================================
 */

#include <Name.h>
#include "Address.h"

// Default constructor
Address::Address() {
    // Nothing needed because std::string has already a default constructor
}

// Destructor
Address::~Address() {
    // Nothing needed because std::string has already a default destructor
}

// Constructor with initializing
Address::Address(string pStreet, string pPostcode, string pCity) {
    street = pStreet;
    postcode = pPostcode;
    city = pCity;
}

// SET METHODS
void Address::setStreet(string pStreet) {
    street = pStreet;
}

void Address::setPostcode(string pPostcode) {
    postcode = pPostcode;
}

void Address::setCity(string pCity) {
    city = pCity;
}

// GET METHODS
string Address::getStreet() const {
    return street;
}

string Address::getPostcode() const {
    return postcode;
}

string Address::getCity() const {
    return city;
}


// OVERLOADS

// Compare (less than)
bool Address::operator< (const Address &address) const
{
    string lhs { (city + " " + street) };
    string rhs { (address.city+ " " + address.street) };

    for (auto &c : lhs) {
        c = tolower(c);
    }
    for (auto &c : rhs) {
        c = tolower(c);
    }

    // Compare
    return (lhs < rhs);
}

// Compare (equal)
bool Address::operator== (const Address &address) const
{
    string lhs { (city + " " + street) };
    string rhs { (address.city + " " + address.street) };

    for (auto &c : lhs) {
        c = tolower(c);
    }
    for (auto &c : rhs) {
        c = tolower(c);
    }

    // Compare
    return (lhs == rhs);
}


// OUTPUT OPERATOR OVERLOAD

// Input operator
std::ostream &operator<< (std::ostream &os, const Address &address)
{
    os << address.getStreet()
    << DELIM
    << address.getPostcode()
    << DELIM
    << address.getCity()
    << DELIM;
    return os;
}

// Input operator overload
std::istream &operator>> (std::istream &is, Address &address)
{
    string temp;
    getline(is,temp,DELIM); // Read in first column
    address.setStreet(temp);

    getline(is,temp,DELIM); // Read in second column
    address.setPostcode(temp);

    getline(is,temp,DELIM); // Read in third column
    address.setCity(temp);
}
