//
// Created by pauli on 2/10/19.
//

#include "PersonList.h"

// Default constructor
PersonList::PersonList() {
    fileName = "../defaultPersonList.txt";
}

// GET FUNCTIONS

//------------------------------------------------------------------------------
// getPerson()
// Return a person from the element with index = idx
//------------------------------------------------------------------------------
bool PersonList::getPerson(int idx, Person &person) const
{
    bool idxOk = false;
    if( idx > -1 && idx < int(persons.size())) {idxOk = true;}

    if(idxOk == true)
    {
        person = persons[idx];
    }
    return idxOk;
}


//------------------------------------------------------------------------------
// getListLength()
// Return number of elements in the list (the vector)
//------------------------------------------------------------------------------
size_t PersonList::getListLength() const
{
    return persons.size();
}

// SET FUNCTIONS

//------------------------------------------------------------------------------
// setFileName()
// set filename of a PersonList-object
//------------------------------------------------------------------------------
void PersonList::setFileName(string &pFileName)
{
    fileName = pFileName;
}

//------------------------------------------------------------------------------
// addPerson
//------------------------------------------------------------------------------
void PersonList::addPerson(Person const &person)
{
    persons.push_back(person);
}


// SORT FUNCTIONS
//------------------------------------------------------------------------------
// sortName()
//------------------------------------------------------------------------------
void PersonList::sortName()
{
    sort(persons.begin(), persons.end());
}


//------------------------------------------------------------------------------
// Help functions for sorting
//------------------------------------------------------------------------------
// personnumber less-than
bool persNrLT(const Person &p1, const Person &p2)
{
    return p1.getPersNr() < p2.getPersNr();
}
// skonummer less-than
bool skoNrLT(const Person &p1, const Person &p2)
{
    return p1.getSkoNr() < p2.getSkoNr();
}
//------------------------------------------------------------------------------
// sortPersnr()
//------------------------------------------------------------------------------
void PersonList::sortPersNr()
{
    sort(persons.begin(), persons.end(), persNrLT);
}

//------------------------------------------------------------------------------
// sortSkoNr()
//------------------------------------------------------------------------------
void PersonList::sortSkoNr()
{
    sort(persons.begin(), persons.end(), skoNrLT);
}


// INPUT-OUTPUT
//------------------------------------------------------------------------------
// readFromFile
//------------------------------------------------------------------------------
void PersonList::readFromFile()
{
    ifstream inf(fileName.c_str());
    //ifstream inf("../Hfile.txt");
    cout << "open?: " << inf.is_open() << endl;
    persons.clear(); //Empty the vector before reading begins
    Person tmpPerson;
    while (inf >> tmpPerson) //TODO: BREAKS ONE PAST LAST INPUT
{
    persons.push_back(tmpPerson);
}
    inf.close();
}
//------------------------------------------------------------------------------
// writeToFile()
//------------------------------------------------------------------------------
void PersonList::writeToFile() const
{
    ofstream of(fileName.c_str());
    for(size_t i=0; i < persons.size(); i++)
        of << persons[i] << endl;
    of.close();
}


// Output operator
std::ostream &operator<< (std::ostream &os, const PersonList &personlist)
{
    for (int idx =0; idx < personlist.getListLength(); idx++ )
    {
        Person person;
        personlist.getPerson(idx, person);
        os << person << endl;
    }
    return os;
}
