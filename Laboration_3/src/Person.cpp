//
// Person-class
// Uses Name-class and Address-class
// Created by pauli on 2/9/19.
//


#include "Person.h"

Person::Person() {
    persNr = "-Unknown-";
    skoNr = 0;
}

Person::Person(Name pName, Address pAddress, string pPersNr, int pSkoNr) {
    name = pName;
    address = pAddress;
    persNr = pPersNr;
    skoNr = pSkoNr;
}

void Person::setName(Name &aName) {
    name = aName;
}

void Person::setAddress(Address pAddress) {
    address = pAddress;
}

void Person::setPersNr(string pPersNr) {
    persNr = pPersNr;
}

void Person::setSkoNr(int pSkoNr) {
    skoNr = pSkoNr;
}

// OVERLOADS

// Compare (equal)
bool Person::operator== (const Person &person) const
{
    return ( name ==  person.name && address == person.address);
}

// Compare (less than)
bool Person::operator< (const Person &person) const
{
    // If names are equal compare address
    if ( name == person.name)
    {
        return address < person.address;
    }
    // Else compare name
    return name < person.name;
}

// OUTPUT OPERATOR OVERLOAD

// Output operator
std::ostream &operator<< (std::ostream &os, const Person &person)
{
    os << person.getName()
       << person.getAddress()
       << person.getPersNr()
       << DELIM
       << person.getSkoNr()
       << DELIM;
    return os;
}

// Input operator overload
std::istream &operator>> (std::istream &is, Person &person)
{
    Name tempName;
    is >> tempName;

    Address tempAdr;
    is >> tempAdr;

    string tempPers;
    getline(is,tempPers,DELIM);

    string tempStr;
    getline(is,tempStr,DELIM);
    int tempSko = std::stoi(tempStr);
    is.get();

    person.setName(tempName);
    person.setAddress(tempAdr);
    person.setPersNr(tempPers);
    person.setSkoNr(tempSko);
}

