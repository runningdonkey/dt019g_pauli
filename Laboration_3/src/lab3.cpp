//------------------------------------------------------------------------------
// Lab3.cpp Objektbaserad programmering i C++
//------------------------------------------------------------------------------

#include "UserInterface.h"

/**
 * Main program
 */
int main()
{
    UserInterface userinterface;
    userinterface.run();
}

