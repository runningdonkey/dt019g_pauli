//
// Created by pauli on 4/7/19.
//

#include "UserInterface.h"

void UserInterface::run()
{
    bool exit {false};
    while (!exit) {
        int selection = showMenu();

        // Validation on selection
        bool selectionOK = true;
        if (selection < 1) {selectionOK = false;}
        if (selection > 8) {selectionOK = false;}

        if (selectionOK) {
            switch (selection) {
                case 1:
                {
                    cout << "Ok, let us add a person to the list." << endl;
                    // first construct person-object from user input
                    Person newPerson = inputPerson();

                    // add this person to the personlist
                    cout << "addperson newperson are: " << newPerson;
                    personlist.addPerson(newPerson);
                    break;
                }
                case 2:
                {
                    cout << "Ok, lets print the list." << endl;
                    cout << "The current number of persons in the list is: " << personlist.getListLength() << endl;
                    cout << personlist;
                    break;
                }
                case 3:
                {
                    cout << "Ok, lets save to file." << endl;
                    saveOnFile();
                    break;
                }
                case 4:
                {
                    cout << "Ok, lets read from file." << endl;
                    readFromFile();
                    break;
                }
                case 5: {
                    cout << "Ok, lets sort by name." << endl;
                    personlist.sortName();
                    break;
                }
                case 6:
                    cout << "Ok, lets sort by social security number." << endl;
                    personlist.sortPersNr();
                    break;
                case 7:
                    cout << "Ok, lets sort by shoe size." << endl;
                    personlist.sortSkoNr();
                    break;
                case 8: // EXIT
                    cout << "Ok program now terminates. I hope you have enjoyed this program." << endl;
                    exit = true;
                    break;
                default:
                    cout << "Sorry, I'm lost... and I don't know how I got here." << endl;
            }
        } else { // if !selectionOK
            cout << "Ups... " << endl;
            cout << "That's not a correct choice. Please input a choice as an integer." << endl;
        }
    }
}

// show menu
int UserInterface::showMenu()
{
    vector<string> menu {
            "1. Add a person",
            "2. Print the list of people",
            "3. Save list to the file",
            "4. Read list from file",
            "5. Sort by name",
            "6. Sort by social security number",
            "7. Sort by shoe size",
            "8. Exit."
    };

    cout << endl << endl << endl ;
    cout << "Laboration 3 menu" << endl;
    for (auto e : menu) {
        cout << e << endl;
    }
    return getInteger("Please input your choice as an integer: ");
}

//------------------------------------------------------------------------------
// Read list from file
//------------------------------------------------------------------------------
void UserInterface::readFromFile()
{
    cout << "*** Read from file ***" << endl << endl;
    cout << "Filename: ";
    string fileName;
    getline(cin,fileName);
    personlist.setFileName(fileName);
    personlist.readFromFile();
}

//------------------------------------------------------------------------------
// Save list to file
//------------------------------------------------------------------------------
void UserInterface::saveOnFile()
{
    cout << "*** Save on file ***" << endl << endl;
    cout << "Filename: ";
    string fileName;
    getline(cin,fileName);
    personlist.setFileName(fileName);
    personlist.writeToFile();
}


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  getInteger
 *  Description:  Ask user for integer input via std.out.
 *        Input:  Questing/instruction to user (as a string)
 *       Output:  The integer entered by the user.
 * =====================================================================================
 */
int UserInterface::getInteger(string question)
{
    int sel;
    while ( true ) {
        cout << question ;
        if (cin >> sel) {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            break;
        } else {
            cout << "Please enter a valid integer" << endl;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
        }
    }
    return sel;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  inputPerson()
 *  Description:  Used create a person, via input from user.
 *                Asks user for first name, last name (strings) and address-info.
 *                Validate ASCII and that DELIM is not used.
 *                Returns a Person-object
 *       Input:   -
 *      Output:   Person-object
 * =====================================================================================
 */
Person UserInterface::inputPerson() {
    Person tempPerson;

    bool inputOK;
    do {
        inputOK = true;
        string firstN, lastN;

        // Input to Name-object
        cout << "Please enter first name: ";
        getline(cin, firstN);
        for (auto &c : firstN) {
            if (c == DELIM) { c = DELIM_SUBSTITUTE; }
            if (c < 32 || c > 126) { inputOK = false; }            // ASCII in range [32; 126]
        }

        cout << "Please enter last name: ";
        getline(cin, lastN);
        for (auto &c : lastN) {
            if (c == DELIM) { c = DELIM_SUBSTITUTE; }
            if (c < 32 || c > 126) { inputOK = false; }            // ASCII in range [32; 126]
        }

        // Input to Address-object
        string streetN, post, cityN;
        cout << "Please enter name of street: ";
        getline(cin, streetN);
        for (auto &c : streetN) {
            if (c == DELIM) { c = DELIM_SUBSTITUTE; }
            if (c < 32 || c > 126) { inputOK = false; }            // ASCII in range [32; 126]
        }

        cout << "Please enter postcode: ";
        getline(cin, post);
        for (auto &c : post) {
            if (c == DELIM) { c = DELIM_SUBSTITUTE; }
            if (c < 32 || c > 126) { inputOK = false; }            // ASCII in range [32; 126]
        }

        cout << "Please enter name of city: ";
        getline(cin, cityN);
        for (auto &c : cityN) {
            if (c == DELIM) { c = DELIM_SUBSTITUTE; }
            if (c < 32 || c > 126) { inputOK = false; }            // ASCII in range [32; 126]
        }

        // Input social security number and shoesize
        string persN;
        cout << "Please enter social security number: ";
        getline(cin, persN);
        for (auto &c : persN) {
            if (c == DELIM) { c = DELIM_SUBSTITUTE; }
            if (c < 32 || c > 126) { inputOK = false; }            // ASCII in range [32; 126]
        }

        int skoN = getInteger("Please enter shoesize: ");

        if (!inputOK) {
            cout << "Please only use ASCII characters (Sorry...)." << endl;
        } else { // that is if all input is OK
            cout << "input is OK." << endl;
            // Create the name-object based on user input
            Name tempName(firstN, lastN);
            cout << "tempName: " << tempName << endl;

            // Create the Address-object based on user input
            Address tempAddr(streetN, post, cityN);
            cout << "tempAddr: " << tempAddr << endl;

            // Create Person-object
            tempPerson.setName(tempName);
            tempPerson.setAddress(tempAddr);
            tempPerson.setPersNr(persN);
            tempPerson.setSkoNr(skoN);
            cout << "tempPerson: " << tempPerson << endl;
        }
/*        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');*/
    } while ( !inputOK );

    cout << "Du indtastede: " << endl << tempPerson << endl;
    return tempPerson;
}
