/*
 * =====================================================================================
 *        Class:  Name
 *  Description:  Definitionfile to the class Name
 *   Created by:  Pauli Baadsager on 2/8/19.
 * =====================================================================================
 */

#include "../include/Name.h"


// Default constructor
Name::Name()
{
    // Don't need anything because default constructor of a string is all ready defined.
}

// Destructor
Name::~Name()
{
    // Don't need anything because default destructor of a string is all ready defined.
}

// Constructor for initializing object
Name::Name(string pFirstName, string pLastName)
{
    firstName = pFirstName;
    lastName = pLastName;
}

// DEFINITION OF MEMBER FUNCTIONS (METHODS)

// Set the firstname of a Name-object
void Name::setFirstName(string pFirstName) {
    firstName = pFirstName;
}

// Set the lastname of a Name-object
void Name::setLastName(string pLastName) {
    lastName = pLastName;
}

// Returns the firstname of a Name-object
string Name::getFirstName() const {
    return firstName;
}

// Returns the lastname of a Name-object
string Name::getLastName() const {
    return lastName;
}

// Compare (less than)
bool Name::operator< (const Name &name) const
{
    string lhs { (lastName + " " + firstName) };
    string rhs { (name.lastName + " " + name.firstName) };

    for (auto &c : lhs) {
        c = tolower(c);
    }
    for (auto &c : rhs) {
        c = tolower(c);
    }

    // Compare
    return (lhs < rhs);
}

// Compare (equal)
bool Name::operator== (const Name &name) const
{
    string lhs { (lastName + " " + firstName) };
    string rhs { (name.lastName + " " + name.firstName) };

    for (auto &c : lhs) {
        c = tolower(c);
    }
    for (auto &c : rhs) {
        c = tolower(c);
    }

    // Compare
    return (lhs == rhs);
}


// Output operator overload
std::ostream &operator<< (std::ostream &os, const Name &name)
{
    os << name.getFirstName()
    << DELIM
    << name.getLastName()
    << DELIM;

    return os;
}

// Input operator overload
std::istream &operator>> (std::istream &is, Name &name)
{
    string temp;
    getline(is,temp,DELIM); // Read in first column
    name.setFirstName(temp);

    getline(is,temp,DELIM); // Read in second column
    name.setLastName(temp);
}
