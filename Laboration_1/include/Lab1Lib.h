//
// Created by pauli on 2/5/19.
//

#ifndef DT019G_LAB1LIB_H
#define DT019G_LAB1LIB_H


#include <ctime>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <limits>
#include <random>
#include <sstream>
#include <string>

using std::begin;
using std::cin;
using std::cout;
using std::default_random_engine;
using std::endl;
using std::numeric_limits;
using std::random_device;
using std::streamsize;
using std::string;
using std::uniform_int_distribution;
using std::setw;
using std::right;
using std::left;

size_t getSizeFromUser(string question);
void fillArray(size_t size, int *arr, default_random_engine e, uniform_int_distribution<int> dist);
void showArray(int *end, int *arr);


#endif //DT019G_LAB1LIB_H
