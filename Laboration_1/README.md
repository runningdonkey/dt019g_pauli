# Laboration 1

## Laborationsdata
Pauli Baadsager (log-in name: paba1801)
Laboration 1. Handed-in: Sat. 15. Jan. 2020

FILER
2 directories, 3 files
```
├── include
│   └── Lab1Lib.h
└── src
    ├── Lab1.cpp
    └── Lab1Lib.cpp
```

## Environment & Tools / Utvecklingsmiljö & Verktyg
Operating system: Linux 5.4.11-arch1-1 GNU/Linux
IDE: JetBrains CLion 2019.3.3
CMake version 3.15.3
Git version 2.25.0

## Purpose / Syfte
Learn to use dynamically created arrays. Use pointers to iterate through arrays. Fill the array with random numbers, and calculate values (like sum) from array.

## Procedures / Genomförande
Use the code in the following form to create array dynamically. 
```
int *arr;
arr = new int(size);
    do some stuff here 
delete(arr);
```

## Discussion / Diskussion
First I made alot of fuctions all itererating through the array. That was bad idea, because its the same array the all iterate through. The solution was to make it simpler and just make a single iteration through the array and collect/calculate everything in this single iteration.
I would prefer to use std::vector instead of arrays.

