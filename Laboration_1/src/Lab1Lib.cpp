//
// Created by pauli on 2/5/19.
//

#include "Lab1Lib.h"

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  getSizeFromUser()
 *  Description:  Ask user to enter an positive integer (determine the size of the array)
 *        Input:  A string (the question to the user)
 *       Output:  A size_t type (ensuring that the system can handle the requested size)
 * =====================================================================================
 */
size_t getSizeFromUser(string question) {
    // size_t selection;
    int selection;
    while ( true ) {
        cout << question << endl;
        if (cin >> selection && selection > 0) {
            break;
        } else {
            cout << "Please enter a valid positive integer" << endl;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
        }
    }
    return selection;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  fillArray()
 *  Description:  Fill an array with random integers with a uniform distribution.
 *        Input:  Size of array, the array (pointer to first element), and random engine.
 *       Output:  -
 * =====================================================================================
 */
void fillArray(size_t size, int *arr, default_random_engine e, uniform_int_distribution<int> dist) {
    auto *end = &arr[size]  ;
    for (auto *it = arr; it != end; ++it) {
        *it = dist(e);
    }
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  showArray()
 *  Description:  Prints the array to screen. Making a pause for each NUMBERS_TO_PAUSE
 *                printed value (assignment ask this to be 200).
 *        Input:  The array (pointer to first element). Pointer to one past last element
 *       Output:  -
 * =====================================================================================
 */
void showArray(int *end, int *arr) {
    cin.get();  // removes endline (ENTER) from inputstream

    // Breaks lines after NUMBERS_PR_ROW numbers. Pause output each NUMBERS_TO_PAUSE numbers.
    constexpr unsigned COLUMN_WIDTH{5};     // Set width of each column
    constexpr int NUMBERS_TO_PAUSE{200};    // Set how many numbers are output before wait for key press
    constexpr int NUMBERS_PR_ROW{10};       // It's nice if NUMBERS_PR_ROW divides NUMBERS_TO_PAUSE.

    // Loop through array with pointer *it
    int i {1};
    for (auto *it = arr; it != end; ++it) {

        // Print out a row of array elements
        cout << setw(COLUMN_WIDTH) << right << *it << "\t ";

        // Create a linebreak after NUMBERS_PR_ROW elements are printed
        if ( i%NUMBERS_PR_ROW == 0 ) {
            cout << endl;
        }

        // Create a pause and wait for user to press Enter
        if ( i%NUMBERS_TO_PAUSE == 0 ) {
            string temp;
            getline(cin, temp);
        }
        i++;
    }
    cout << endl;
}
