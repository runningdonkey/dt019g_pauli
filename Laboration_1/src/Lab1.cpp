/*
 * =====================================================================================
 *
 *       Filename:  Lab1.cpp
 *
 *    Description:  Laboration 1 (dt019g: Objektbaserad programmering i C++).
 *                  Using pointers to loop through C-style array with integers.
 *                  Searching for smallest value in array, and largest element in array.
 *                  Calculates sum of values in the array.
 *                  Let user specify the size of the array.
 *
 *        Version:  1.0
 *        Created:  2019-02-07 15:17:57
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Pauli Baadsager (), paulib@pm.me
 *   Organization:  University of the Faroe Islands
 *
 * =====================================================================================
 */

#include "Lab1Lib.h"

int main()
{

    // DECLARATIONS
    constexpr int MIN{-5000};
    constexpr int MAX{5000};

    // Generate random numbers
    random_device rd;
    static default_random_engine e(static_cast<signed long>(rd()));
    static uniform_int_distribution<int> dist(MIN,MAX);

    // Let user specify size of array
    size_t size = getSizeFromUser("How large an array do you want (please enter a positive integer)?: ");

    // Declare a dynamic array
    int *arr;
    arr = new int(size);

    // Fill an array with random integers
    fillArray(size, arr, e, dist);


    // Loop through array with pointer *it
    int *it = arr;
    int *last = &arr[size];     // Pointer to one past last element of array
    int *largest = arr;     // pointer to largest element, initialed to the first element of the array
    int *smallest = arr;      // pointer to smallest element, initialed to the first element of the array
    auto sum {0};

    while (*it < *last) {
        sum += *it;     // calculate sum

        // If element is bigger than current largest, then largest-pointer moves to that element
        if ( *it > *largest) {
            largest = it;
        }

        // If element is less than current smallest, then smallest-pointer moves to that element.
        if ( *it < *smallest) {
            smallest = it;
        }
        ++it;
    }

    showArray(last, arr);
    //
    cout << "The array size you gave was: " << size << endl;
    cout << "The largest value in the array is: " << *largest << endl;
    cout << "The smallest value in the array is: " << *smallest << endl;
    cout << "The sum of the elements in the array is: " << sum << endl;

    // Free memory
    delete (arr);
}

